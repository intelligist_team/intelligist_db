-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: node1
-- Generation Time: Jul 02, 2018 at 06:14 AM
-- Server version: 10.1.23-MariaDB-1~jessie
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intelligist`
--
CREATE DATABASE IF NOT EXISTS `intelligist` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `intelligist`;

-- --------------------------------------------------------

--
-- Table structure for table `categories_list`
--

CREATE TABLE `categories_list` (
  `categories_id` int(11) NOT NULL,
  `categories_name` varchar(255) NOT NULL,
  `owner` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories_permission`
--

CREATE TABLE `categories_permission` (
  `cate_perm_id` int(11) NOT NULL,
  `categories_id` varchar(10) NOT NULL,
  `user_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `codename_download_type`
--

CREATE TABLE `codename_download_type` (
  `download_type_code` int(11) NOT NULL,
  `download_type_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codename_download_type`
--

INSERT INTO `codename_download_type` (`download_type_code`, `download_type_name`) VALUES
(1, 'WEB Page'),
(2, 'Excel File'),
(3, 'CSV File'),
(4, 'Text File'),
(5, 'XML File'),
(6, 'Web Service'),
(7, 'Manual Generate');

-- --------------------------------------------------------

--
-- Table structure for table `codename_import_status`
--

CREATE TABLE `codename_import_status` (
  `import_status_code` int(11) NOT NULL,
  `import_status_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codename_import_status`
--

INSERT INTO `codename_import_status` (`import_status_code`, `import_status_name`) VALUES
(1, 'Error'),
(2, 'Pending'),
(3, 'Complete');

-- --------------------------------------------------------

--
-- Table structure for table `codename_privilege_level`
--

CREATE TABLE `codename_privilege_level` (
  `privilege_level_code` int(11) NOT NULL,
  `privilege_level_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codename_privilege_level`
--

INSERT INTO `codename_privilege_level` (`privilege_level_code`, `privilege_level_name`) VALUES
(1, 'RootAdmin'),
(2, 'Admin'),
(3, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `codename_schedule_mode`
--

CREATE TABLE `codename_schedule_mode` (
  `schedule_mode_code` int(11) NOT NULL,
  `schedule_mode_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codename_schedule_mode`
--

INSERT INTO `codename_schedule_mode` (`schedule_mode_code`, `schedule_mode_name`) VALUES
(1, 'None'),
(2, 'Daily'),
(3, 'Day Of Month'),
(4, 'End Of Month'),
(5, 'First day of the month');

-- --------------------------------------------------------

--
-- Table structure for table `codename_status`
--

CREATE TABLE `codename_status` (
  `status_code` int(11) NOT NULL,
  `status_name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codename_status`
--

INSERT INTO `codename_status` (`status_code`, `status_name`) VALUES
(1, 'Inactive'),
(2, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `codename_user_status`
--

CREATE TABLE `codename_user_status` (
  `status_code` int(11) NOT NULL,
  `status_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `codename_user_status`
--

INSERT INTO `codename_user_status` (`status_code`, `status_name`) VALUES
(1, 'Offline'),
(2, 'Online'),
(3, 'Suspended'),
(4, 'Pending'),
(5, 'New');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_list`
--

CREATE TABLE `dashboard_list` (
  `dashboard_id` int(11) NOT NULL,
  `dashboard_name` varchar(255) NOT NULL,
  `owner` varchar(10) NOT NULL,
  `link` text NOT NULL,
  `categories_id` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_permission`
--

CREATE TABLE `dashboard_permission` (
  `dash_perm_id` int(11) NOT NULL,
  `dashboard_id` varchar(10) NOT NULL,
  `user_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_uac`
--

CREATE TABLE `dashboard_uac` (
  `id` int(11) NOT NULL,
  `uid` varchar(10) NOT NULL,
  `dashboard_available` varchar(255) NOT NULL,
  `sub_dashboard_available` varchar(255) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `list_group`
--

CREATE TABLE `list_group` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `owner_uid` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `list_role`
--

CREATE TABLE `list_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `menu_available` varchar(255) NOT NULL,
  `owner_uid` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `metadataTable`
--

CREATE TABLE `metadataTable` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `source_name` varchar(255) NOT NULL,
  `download_type` varchar(2) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT 'Active/Inactive',
  `webaddr` text COMMENT 'web address',
  `localpath` text,
  `header_file` text,
  `append_mode` varchar(1) NOT NULL,
  `ref` varchar(255) DEFAULT NULL COMMENT 'reference',
  `schedule_mode` varchar(2) NOT NULL,
  `schedule_start` datetime DEFAULT NULL,
  `auto_approve` varchar(1) NOT NULL,
  `auto_generate` varchar(1) NOT NULL,
  `import_status` varchar(1) NOT NULL,
  `log_message` text NOT NULL,
  `trans_date` datetime NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `owner_user`
--

CREATE TABLE `owner_user` (
  `id` int(11) NOT NULL,
  `owner_uid` varchar(10) NOT NULL,
  `user_uid` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pre_add_group`
--

CREATE TABLE `pre_add_group` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `group_id` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pre_add_owner_user`
--

CREATE TABLE `pre_add_owner_user` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `owner_uid` varchar(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pre_add_role`
--

CREATE TABLE `pre_add_role` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `role_id` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `token_forgotpassword`
--

CREATE TABLE `token_forgotpassword` (
  `token_id` int(11) NOT NULL,
  `token` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `is_active` varchar(20) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `token_register`
--

CREATE TABLE `token_register` (
  `token_id` int(11) NOT NULL,
  `token` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `privilege_level` varchar(2) DEFAULT NULL,
  `job_title` varchar(255) NOT NULL,
  `status` varchar(2) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_list_group`
--

CREATE TABLE `user_list_group` (
  `id` int(11) NOT NULL,
  `group_id` varchar(255) NOT NULL,
  `uid` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_list_role`
--

CREATE TABLE `user_list_role` (
  `id` int(11) NOT NULL,
  `role_id` varchar(255) NOT NULL,
  `uid` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories_list`
--
ALTER TABLE `categories_list`
  ADD PRIMARY KEY (`categories_id`);

--
-- Indexes for table `categories_permission`
--
ALTER TABLE `categories_permission`
  ADD PRIMARY KEY (`cate_perm_id`);

--
-- Indexes for table `codename_download_type`
--
ALTER TABLE `codename_download_type`
  ADD PRIMARY KEY (`download_type_code`);

--
-- Indexes for table `codename_privilege_level`
--
ALTER TABLE `codename_privilege_level`
  ADD PRIMARY KEY (`privilege_level_code`);

--
-- Indexes for table `codename_schedule_mode`
--
ALTER TABLE `codename_schedule_mode`
  ADD PRIMARY KEY (`schedule_mode_code`);

--
-- Indexes for table `codename_status`
--
ALTER TABLE `codename_status`
  ADD PRIMARY KEY (`status_code`);

--
-- Indexes for table `codename_user_status`
--
ALTER TABLE `codename_user_status`
  ADD PRIMARY KEY (`status_code`);

--
-- Indexes for table `dashboard_list`
--
ALTER TABLE `dashboard_list`
  ADD PRIMARY KEY (`dashboard_id`);

--
-- Indexes for table `dashboard_permission`
--
ALTER TABLE `dashboard_permission`
  ADD PRIMARY KEY (`dash_perm_id`);

--
-- Indexes for table `dashboard_uac`
--
ALTER TABLE `dashboard_uac`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_group`
--
ALTER TABLE `list_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `list_role`
--
ALTER TABLE `list_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `metadataTable`
--
ALTER TABLE `metadataTable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_user`
--
ALTER TABLE `owner_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pre_add_group`
--
ALTER TABLE `pre_add_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pre_add_owner_user`
--
ALTER TABLE `pre_add_owner_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pre_add_role`
--
ALTER TABLE `pre_add_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token_forgotpassword`
--
ALTER TABLE `token_forgotpassword`
  ADD PRIMARY KEY (`token_id`);

--
-- Indexes for table `token_register`
--
ALTER TABLE `token_register`
  ADD PRIMARY KEY (`token_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_list_group`
--
ALTER TABLE `user_list_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_list_role`
--
ALTER TABLE `user_list_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories_list`
--
ALTER TABLE `categories_list`
  MODIFY `categories_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `categories_permission`
--
ALTER TABLE `categories_permission`
  MODIFY `cate_perm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `codename_download_type`
--
ALTER TABLE `codename_download_type`
  MODIFY `download_type_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `codename_privilege_level`
--
ALTER TABLE `codename_privilege_level`
  MODIFY `privilege_level_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `codename_schedule_mode`
--
ALTER TABLE `codename_schedule_mode`
  MODIFY `schedule_mode_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `codename_status`
--
ALTER TABLE `codename_status`
  MODIFY `status_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `codename_user_status`
--
ALTER TABLE `codename_user_status`
  MODIFY `status_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dashboard_list`
--
ALTER TABLE `dashboard_list`
  MODIFY `dashboard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `dashboard_permission`
--
ALTER TABLE `dashboard_permission`
  MODIFY `dash_perm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=274;

--
-- AUTO_INCREMENT for table `dashboard_uac`
--
ALTER TABLE `dashboard_uac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `list_group`
--
ALTER TABLE `list_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `list_role`
--
ALTER TABLE `list_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `metadataTable`
--
ALTER TABLE `metadataTable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT for table `owner_user`
--
ALTER TABLE `owner_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `pre_add_group`
--
ALTER TABLE `pre_add_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pre_add_owner_user`
--
ALTER TABLE `pre_add_owner_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `pre_add_role`
--
ALTER TABLE `pre_add_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `token_forgotpassword`
--
ALTER TABLE `token_forgotpassword`
  MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `token_register`
--
ALTER TABLE `token_register`
  MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `user_list_group`
--
ALTER TABLE `user_list_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `user_list_role`
--
ALTER TABLE `user_list_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- 9f3f2cea26ccd91a343edd63d87200e3ef71d4438619a3b3c67d094dbfa70c03:intelligist
-- 6031a764f2ee50f9d472d18f1b722e9b277eec0751e18fb8bcba76543a64024d:POLL#Admin